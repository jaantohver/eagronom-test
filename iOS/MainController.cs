﻿using System;
using System.Timers;
using System.Collections.Generic;

using UIKit;

namespace eAgronom_test.iOS
{
    public class MainController : UIViewController
    {
        Timer updateTimer;

        const double ToastDuration = 2;

        MainView contentView;

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Title = "Main";

            View = contentView = new MainView();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            contentView.GetButton.TouchUpInside += GetButtonHandler;
            contentView.PostButton.TouchUpInside += PostButtonHandler;

            contentView.Messages.Text = "Loading...";
            UpdateMessages();

            updateTimer = new Timer(2500);
            updateTimer.Elapsed += UpdateAll;
            updateTimer.Start();
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            contentView.GetButton.TouchUpInside -= GetButtonHandler;
            contentView.PostButton.TouchUpInside -= PostButtonHandler;

            updateTimer.Stop();
        }

        void GetButtonHandler(object sender, EventArgs e)
        {
            contentView.Messages.Text = "Loading...";

            UpdateMessages();
        }

        async void PostButtonHandler(object sender, EventArgs e)
        {
            UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

            if (contentView.MessageText.Text.Trim().Length > 0)
            {
                Message message = new Message(contentView.MessageText.Text);

                Response response = await Networking.PostMessage(message);

                if (response.Status == ResponseStatus.Success)
                {
                    contentView.ShowToastWithDuration("Message posted successfully!", ToastDuration);

                    await SQLiteClient.Instance.AddSentMessage(message);
                }
                else
                {
                    await SQLiteClient.Instance.AddUnsentMessage(message);
                }
            }

            UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
        }

        async void UpdateMessages()
        {
            UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

            GetResponse response = await Networking.GetMessages();

            if (response.Status == ResponseStatus.Success)
            {
                contentView.HideToast();

                if (response.Messages.Count > 0)
                {
                    InvokeOnMainThread(() => { contentView.Messages.Text = string.Empty; });

                    DisplayMessages(response.Messages);
                }
                else
                {
                    InvokeOnMainThread(() => { contentView.Messages.Text = "No messages"; });
                }

                await SQLiteClient.Instance.UpdateReceivedMessages(response.Messages);

                var a = await SQLiteClient.Instance.GetAllReceivedMessages();
                Console.WriteLine(a.Count);
            }
            else if (response.Status == ResponseStatus.NoNetwork)
            {
                List<Message> messages = await SQLiteClient.Instance.GetAllReceivedMessages();

                DisplayMessages(messages);

                contentView.ShowToast(response.Status.GetStringValue());
            }
            else
            {
                List<Message> messages = await SQLiteClient.Instance.GetAllReceivedMessages();

                DisplayMessages(messages);

                contentView.ShowToastWithDuration(response.Status.GetStringValue(), ToastDuration);
            }

            UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
        }

        void UpdateAll(object sender, EventArgs e)
        {
            UpdateMessages();
            RetryMessagePost();
        }

        async void RetryMessagePost()
        {
            UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

            List<Message> messages = await SQLiteClient.Instance.GetAllUnsentMessages();

            int updateCount = 0;

            foreach (Message m in messages)
            {
                Response r = await Networking.PostMessage(m);

                if (r.Status == ResponseStatus.Success)
                {
                    updateCount++;

                    await SQLiteClient.Instance.MarkMessageAsSent(m);

                    contentView.ShowOrUpdateToast("Sent post messages (" + updateCount + ")");
                }
            }
        }


        void DisplayMessages(List<Message> messages)
        {
            foreach (Message m in messages)
            {
                InvokeOnMainThread(() => { contentView.Messages.Text += m.Content + "\n"; });
            }
        }
    }
}