﻿using System;
using System.Timers;

using UIKit;
using CoreGraphics;

namespace eAgronom_test.iOS
{
    public class MainView : UIView
    {
        bool toastVisible = false;
        bool viewsInitialized = false;

        Timer animationTimer = new Timer();

        readonly UILabel Toast;
        public readonly UIButton GetButton, PostButton;
        public readonly UITextView Messages;
        public readonly UITextField MessageText;

        public MainView()
        {
            BackgroundColor = UIColor.LightGray;

            GetButton = new UIButton(UIButtonType.System);
            GetButton.SetTitle("Manual GET", UIControlState.Normal);

            MessageText = new UITextField();
            MessageText.BackgroundColor = UIColor.White;
            MessageText.Placeholder = "Message";

            PostButton = new UIButton(UIButtonType.System);
            PostButton.SetTitle("POST", UIControlState.Normal);

            Messages = new UITextView();
            Messages.BackgroundColor = UIColor.White;
            Messages.Editable = false;

            Toast = new UILabel();
            Toast.BackgroundColor = UIColor.Black;
            Toast.TextColor = UIColor.White;
            Toast.TextAlignment = UITextAlignment.Center;

            AddSubviews(GetButton, MessageText, PostButton, Messages, Toast);
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            if (!viewsInitialized)
            {
                nfloat padding = 25;
                nfloat width = Frame.Width - padding * 2;

                GetButton.SizeToFit();
                GetButton.Frame = new CGRect(
                    padding,
                    padding,
                    width,
                    GetButton.Frame.Height
                );

                MessageText.SizeToFit();
                MessageText.Frame = new CGRect(
                    padding,
                    GetButton.Frame.Bottom + padding,
                    width,
                    MessageText.Frame.Height
                );

                PostButton.SizeToFit();
                PostButton.Frame = new CGRect(
                    padding,
                    MessageText.Frame.Bottom + padding,
                    width,
                    PostButton.Frame.Height
                );

                Messages.Frame = new CGRect(
                    padding,
                    PostButton.Frame.Bottom + padding,
                    width,
                    Frame.Height - (PostButton.Frame.Bottom + padding) - padding
                );

                Toast.Frame = new CGRect(
                    0,
                    Frame.Height,
                    Frame.Width,
                    50
                );
            }
        }

        public void ShowToast(string message)
        {
            InvokeOnMainThread(() =>
            {
                Toast.Text = message;

                AnimateNotify(0.5, () =>
                {
                    Toast.Frame = new CGRect(
                        Toast.Frame.X,
                        Frame.Height - Toast.Frame.Height,
                        Toast.Frame.Width,
                        Toast.Frame.Height
                    );
                }, (success) => toastVisible = true);
            });
        }

        public void HideToast()
        {
            InvokeOnMainThread(() =>
            {
                AnimateNotify(0.5, () =>
                {
                    Toast.Frame = new CGRect(
                        Toast.Frame.X,
                        Frame.Height,
                        Toast.Frame.Width,
                        Toast.Frame.Height
                    );
                }, (success) => toastVisible = false);
            });
        }

        public void ShowToastWithDuration(string message, double seconds)
        {
            animationTimer.Stop();

            ShowToast(message);

            animationTimer = new Timer(seconds * 1000);
            animationTimer.AutoReset = false;
            animationTimer.Elapsed += (sender, e) =>
            {
                HideToast();
            };
            animationTimer.Start();
        }

        public void ShowOrUpdateToast(string message)
        {
            if (!toastVisible)
            {
                ShowToastWithDuration(message, 2);
            }
            else
            {
                animationTimer.Stop();
                animationTimer.Start();

                InvokeOnMainThread(() => { Toast.Text = message; });
            }
        }
    }
}