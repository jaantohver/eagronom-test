﻿using UIKit;

namespace eAgronom_test.iOS
{
    public class Application
    {
        static void Main(string[] args)
        {
            UIApplication.Main(args, null, typeof(AppDelegate));
        }
    }
}