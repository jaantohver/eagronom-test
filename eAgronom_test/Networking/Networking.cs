﻿using System;
using System.Net;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static System.Diagnostics.Debug;

namespace eAgronom_test
{
    public static class Networking
    {
        const string Url = "https://webhook.site/32b13436-fa15-4bf2-a02f-541fb3e9e217";

        static bool networkAvailable = false;

        static readonly HttpClient client;

        static Networking()
        {
            client = new HttpClient();
        }

        public static async Task<GetResponse> GetTest()
        {
            GetResponse response = new GetResponse();
            List<Message> messages = new List<Message>();

            string url = "https://www.google.com";

            try
            {
                using (HttpResponseMessage responseMessage = await client.GetAsync(url))
                using (HttpContent content = responseMessage.Content)
                {
                    string responseString = await content?.ReadAsStringAsync();

                    networkAvailable = true;

                    if (responseMessage.StatusCode == HttpStatusCode.OK && responseString != null)
                    {
                        //webhook.site for some reason does not return the Content-Type header even if specified. This
                        //attempts to parse the response as JSON and assumes a single plaintext message otherwise

                        try
                        {
                            JObject json = JObject.Parse(responseString);

                            messages = Codec.ParseMessages(json);
                        }
                        catch (JsonReaderException)
                        {
                            messages.Add(new Message(responseString));
                        }

                        response.Status = ResponseStatus.Success;
                    }
                    else
                    {
                        response.Status = ResponseStatus.UnknownError;
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine(e);
                if (e is HttpRequestException)
                {
                    WriteLine("[" + nameof(GetMessages) + "] No network");

                    response.Status = ResponseStatus.NoNetwork;

                    networkAvailable = false;

                }
                else if (e is WebException)
                {
                    WriteLine("[" + nameof(GetMessages) + "] No network");

                    response.Status = ResponseStatus.NoNetwork;

                    networkAvailable = false;
                }
                else if (e is TaskCanceledException)
                {
                    WriteLine("[" + nameof(GetMessages) + "] Timeout reached");

                    response.Status = ResponseStatus.Timeout;
                }
                else
                {
                    response.Status = ResponseStatus.UnknownError;

#if DEBUG
                    throw;
#else
                    //Obviously log this somewhere. Currently out of scope.
#endif
                }
            }

            response.Messages = messages;

            return response;
        }

        public static async Task<GetResponse> GetMessages()
        {
            GetResponse response = new GetResponse();
            List<Message> messages = new List<Message>();

            string url = Url;

            try
            {
                using (HttpResponseMessage responseMessage = await client.GetAsync(url))
                using (HttpContent content = responseMessage.Content)
                {
                    string responseString = await content?.ReadAsStringAsync();

                    networkAvailable = true;

                    if (responseMessage.StatusCode == HttpStatusCode.OK && responseString != null)
                    {
                        //webhook.site for some reason does not return the Content-Type header even if specified. This
                        //attempts to parse the response as JSON and assumes a single plaintext message otherwise

                        try
                        {
                            JObject json = JObject.Parse(responseString);

                            messages = Codec.ParseMessages(json);
                        }
                        catch (JsonReaderException)
                        {
                            messages.Add(new Message(responseString));
                        }

                        response.Status = ResponseStatus.Success;
                    }
                    else
                    {
                        response.Status = ResponseStatus.UnknownError;
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine(e);
                if (e is HttpRequestException)
                {
                    WriteLine("[" + nameof(GetMessages) + "] No network");

                    response.Status = ResponseStatus.NoNetwork;

                    networkAvailable = false;

                }
                else if (e is WebException)
                {
                    WriteLine("[" + nameof(GetMessages) + "] No network");

                    response.Status = ResponseStatus.NoNetwork;

                    networkAvailable = false;
                }
                else if (e is TaskCanceledException)
                {
                    WriteLine("[" + nameof(GetMessages) + "] Timeout reached");

                    response.Status = ResponseStatus.Timeout;
                }
                else
                {
                    response.Status = ResponseStatus.UnknownError;

#if DEBUG
                    throw;
#else
                    //Obviously log this somewhere. Currently out of scope.
#endif
                }
            }

            response.Messages = messages;

            return response;
        }

        public static async Task<Response> PostMessage(Message message)
        {
            PostResponse response = new PostResponse();

            string url = Url;

            JObject json = Codec.EncodeMessage(message);

            StringContent body = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

            try
            {
                using (HttpResponseMessage responseMessage = await client.PostAsync(url, body))
                using (HttpContent content = responseMessage.Content)
                {
                    networkAvailable = true;

                    if (responseMessage.StatusCode == HttpStatusCode.OK)
                    {
                        //No response currently. Expected that 200 status means a successful post.

                        response.Status = ResponseStatus.Success;
                    }
                    else
                    {
                        response.Status = ResponseStatus.UnknownError;
                    }
                }
            }
            catch (Exception e)
            {
                if (e is HttpRequestException)
                {
                    WriteLine("[" + nameof(PostMessage) + "] No network");

                    response.Status = ResponseStatus.NoNetwork;

                    networkAvailable = false;
                }
                else if (e is WebException)
                {
                    WriteLine("[" + nameof(GetMessages) + "] No network");

                    response.Status = ResponseStatus.NoNetwork;

                    networkAvailable = false;
                }
                else if (e is TaskCanceledException)
                {
                    WriteLine("[" + nameof(PostMessage) + "] Timeout reached");

                    response.Status = ResponseStatus.Timeout;
                }
                else
                {
                    response.Status = ResponseStatus.UnknownError;

#if DEBUG
                    throw;
#else
                    //Obviously log this somewhere. Currently out of scope.
#endif
                }
            }

            return response;
        }
    }
}