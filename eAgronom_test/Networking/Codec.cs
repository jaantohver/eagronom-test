﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;

using static System.Diagnostics.Debug;

namespace eAgronom_test
{
    public static class Codec
    {
        public static List<Message> ParseMessages(JObject json)
        {
            List<Message> messages = new List<Message>();

            if (json.ContainsKey("messages") && json["messages"].GetType() == typeof(JArray))
            {
                foreach (JToken a in json["messages"] as JArray)
                {
                    messages.Add(new Message(a.Value<string>()));
                }
            }

            return messages;
        }

        public static JObject EncodeMessage(Message message)
        {
            JObject json = new JObject
            {
                ["message"] = message.Content
            };

            return json;
        }
    }
}