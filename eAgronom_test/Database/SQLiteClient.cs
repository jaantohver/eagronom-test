﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;

using SQLite;

namespace eAgronom_test
{
    public class SQLiteClient
    {
        readonly SQLiteConnection connection;

        public SQLiteClient(string folder)
        {
            //string folder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string path = Path.Combine(folder, "database.db3");

            connection = new SQLiteConnection(path);

            //db.DropTable(new TableMapping(typeof(Message)));
            connection.CreateTable<Message>();
        }

        public void InsertMessage(Message message)
        {
            connection.Insert(message);
        }

        public Task<List<Message>> GetAllMessages()
        {
            return Task.Factory.StartNew(() =>
            {
                return connection.Query<Message>("SELECT * FROM Message");
            });
        }

        public Task<List<Message>> GetAllSentMessages()
        {
            return Task.Factory.StartNew(() =>
            {
                int status = (int)MessageStatus.Sent;

                return connection.Query<Message>("SELECT * FROM Message WHERE Status=?", status);
            });
        }

        public Task<List<Message>> GetAllUnsentMessages()
        {
            return Task.Factory.StartNew(() =>
            {
                int status = (int)MessageStatus.Unsent;

                return connection.Query<Message>("SELECT * FROM Message WHERE Status=?", status);
            });
        }

        public Task<List<Message>> GetAllReceivedMessages()
        {
            return Task.Factory.StartNew(() =>
            {
                int status = (int)MessageStatus.Received;

                return connection.Query<Message>("SELECT * FROM Message WHERE Status=?", status);
            });
        }

        public Task<int> UpdateReceivedMessages(List<Message> messages)
        {
            //Not ideal but we have no timestamps or cursor based queries.
            //Assuming the volume of messages is low, this is fine as is.
            return Task.Factory.StartNew(() =>
            {
                int status = (int)MessageStatus.Received;

                connection.Query<Message>("DELETE FROM Message WHERE Status=?", status);

                foreach (Message m in messages)
                {
                    m.Status = MessageStatus.Received;
                }

                return connection.InsertAll(messages);
            });
        }

        public Task<int> AddSentMessage(Message message)
        {
            return Task.Factory.StartNew(() =>
            {
                message.Status = MessageStatus.Sent;

                return connection.Insert(message);
            });
        }

        public Task<int> AddUnsentMessage(Message message)
        {
            return Task.Factory.StartNew(() =>
            {
                message.Status = MessageStatus.Unsent;

                return connection.Insert(message);
            });
        }

        public Task MarkMessageAsSent(Message message)
        {
            return Task.Factory.StartNew(() =>
            {
                message.Status = MessageStatus.Sent;

                return connection.Update(message);
            });
        }
    }
}