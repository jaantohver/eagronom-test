﻿using System;

namespace eAgronom_test
{
    public enum MessageStatus
    {
        Sent = 1,
        Unsent = 2,
        Received = 3
    }
}