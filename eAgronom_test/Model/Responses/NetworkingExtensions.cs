﻿using System;

namespace eAgronom_test
{
    public static class NetworkingExtensions
    {
        public static string GetStringValue(this ResponseStatus status)
        {
            switch (status)
            {
                case ResponseStatus.Success:
                    return "Success";
                case ResponseStatus.NoNetwork:
                    return "No network connection";
                case ResponseStatus.Timeout:
                    return "Request timeout";
                case ResponseStatus.UnknownError:
                    return "An unknown error occurred";
                default:
                    return string.Empty;
            }
        }
    }
}
