﻿using System.Collections.Generic;

namespace eAgronom_test
{
    public class GetResponse : Response
    {
        public List<Message> Messages { get; set; }
    }
}