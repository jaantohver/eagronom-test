﻿using System;

namespace eAgronom_test
{
    public enum ResponseStatus
    {
        Success,
        NoNetwork,
        Timeout,
        UnknownError
    }
}
