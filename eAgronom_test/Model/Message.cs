﻿using SQLite;

namespace eAgronom_test
{
    public class Message
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Content { get; set; }

        public MessageStatus Status { get; set; }

        public Message()
        {
        }

        public Message(string content)
        {
            Content = content;
        }
    }
}