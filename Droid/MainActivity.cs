﻿using System;
using System.Timers;
using System.Collections.Generic;

using Android.OS;
using Android.App;
using Android.Widget;

namespace eAgronom_test.Droid
{
    [Activity(Label = "Main", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        Timer updateTimer;

        const double ToastDuration = 2;

        Button getButton, postButton;
        TextView messagesView;
        EditText messageText;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);

            getButton = FindViewById<Button>(Resource.Id.getButton);

            messageText = FindViewById<EditText>(Resource.Id.messageText);

            postButton = FindViewById<Button>(Resource.Id.postButton);

            messagesView = FindViewById<TextView>(Resource.Id.messages);
        }

        protected override void OnResume()
        {
            base.OnResume();

            getButton.Click += GetButtonHandler;
            postButton.Click += PostButtonHandler;

            messagesView.Text = "Loading...";
            UpdateMessages();

            updateTimer = new Timer(2500);
            updateTimer.Elapsed += UpdateAll;
            updateTimer.Start();
        }

        protected override void OnPause()
        {
            base.OnPause();

            getButton.Click -= GetButtonHandler;
            postButton.Click -= PostButtonHandler;

            updateTimer.Stop();
        }

        void GetButtonHandler(object sender, EventArgs e)
        {
            messagesView.Text = "Loading...";

            UpdateMessages();
        }

        async void PostButtonHandler(object sender, EventArgs e)
        {
            if (messageText.Text.Trim().Length > 0)
            {
                Message message = new Message(messageText.Text);

                Response response = await Networking.PostMessage(message);

                if (response.Status == ResponseStatus.Success)
                {
                    RunOnUiThread(() => Toast.MakeText(this, "Message posted successfully!", ToastLength.Short).Show());

                    await SQLiteClient.Instance.AddSentMessage(message);
                }
                else
                {
                    await SQLiteClient.Instance.AddUnsentMessage(message);
                }
            }
        }

        async void UpdateMessages()
        {
            GetResponse response = await Networking.GetMessages();
            //GetResponse response = await Networking.GetTest();

            if (response.Status == ResponseStatus.Success)
            {
                if (response.Messages.Count > 0)
                {
                    RunOnUiThread(() => { messagesView.Text = string.Empty; });

                    DisplayMessages(response.Messages);
                }
                else
                {
                    RunOnUiThread(() => { messagesView.Text = "No messages"; });
                }

                await SQLiteClient.Instance.UpdateReceivedMessages(response.Messages);

                var a = await SQLiteClient.Instance.GetAllReceivedMessages();
                Console.WriteLine(a.Count);
            }
            else if (response.Status == ResponseStatus.NoNetwork)
            {
                List<Message> messages = await SQLiteClient.Instance.GetAllReceivedMessages();

                DisplayMessages(messages);

                RunOnUiThread(() => Toast.MakeText(this, response.Status.GetStringValue(), ToastLength.Short).Show());
            }
            else
            {
                List<Message> messages = await SQLiteClient.Instance.GetAllReceivedMessages();

                DisplayMessages(messages);

                RunOnUiThread(() => Toast.MakeText(this, response.Status.GetStringValue(), ToastLength.Short).Show());
            }
        }

        void UpdateAll(object sender, EventArgs e)
        {
            UpdateMessages();
            RetryMessagePost();
        }

        async void RetryMessagePost()
        {
            List<Message> messages = await SQLiteClient.Instance.GetAllUnsentMessages();

            int updateCount = 0;

            foreach (Message m in messages)
            {
                Response r = await Networking.PostMessage(m);

                if (r.Status == ResponseStatus.Success)
                {
                    updateCount++;

                    await SQLiteClient.Instance.MarkMessageAsSent(m);

                    RunOnUiThread(() => Toast.MakeText(this, "Sent post messages (" + updateCount + ")", ToastLength.Short).Show());
                }
            }
        }


        void DisplayMessages(List<Message> messages)
        {
            foreach (Message m in messages)
            {
                RunOnUiThread(() => { messagesView.Text += m.Content + "\n"; });
            }
        }
    }
}